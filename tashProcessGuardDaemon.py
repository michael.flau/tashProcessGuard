#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
TashProcessGuardDaemon -- A python watchdog application designed
to automatically watch over the liftetime of tashProcessGuard.py
instances. This daemon is supposed to run as part of systemd as a service.

Copyright: 2018, Michael Flau <michael@flau.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import time
import subprocess
import pathlib
import logging
from sys import exit
from os.path import isfile
from systemd.journal import JournalHandler

"""
Check if tashProcessGuard (tPG) instance is stale by comapring last written
timestamp in guard file with current timestamp.
If timestamp is above a defined level, regard tPG as stale and restart it.
"""
def isProcessGuardStale(path, logger, mud):
    #TODO: Check if path, logger, mtd is valid
    p = path

    #Append slash if none is present
    if p[-1] != '/':
        p += '/'

    #Assume process guard is not running if no gurad file is present
    if not isfile( p + 'guard.ts'):
        return False

    #Read guard file
    with open(p + 'guard.ts', 'rt') as f:
        c = f.readline()

    # get current unix timestamp
    cn          = time.time()
    ret         = False
    update_diff = cn - int(c)

    if update_diff > mud:
        logger.info("Last update was too long ago (diff: {} secs), assuming watchdog is stale...restarting".format(update_diff))
        ret = True
    else:
        logger.info("Last update within update limit (diff: {} secs), assuming watchdog is running...".format(update_diff))

    return ret

if __name__ == "__main__":

    # Default path for tashProcessGuard context
    path        = '/tmp/tashtego_pids/'
    # Command to execute in child process
    execpath    = 'xterm -rv -T tashProcessGuard -fa Monospace -fs 8 -geometry 80x10+200+0 -e /home/tashtego/tashtego/bin/tashProcessGuard.py'
    # Create a systemd logger
    logger      = logging.getLogger('tashpgDaemon_logger')
    logger.addHandler(JournalHandler())
    logger.setLevel(logging.INFO)
    logger.info("Starting tashpgDaemon...")
    #time to sleep
    tts = 10
    #max update diff
    mud = 15

    """
    Sleep before entering and let tashProcessGuard write it's first guard file update,
    to not start a second instance of process guard
    """
    time.sleep(tts)

    while 1:

        logger.info("Checking on tashProcessGuard...")

        if isProcessGuardStale( path, logger, mud ):
            logger.info("Restarting processguard...")
            subprocess.Popen(execpath.split(' '))
            logger.info("Waiting for watchdog to init itself...")
            time.sleep(tts)
        else:
            logger.info("...tashProcessGuard is working correctly!")

        logger.info('Daemon sleeps a little...')
        time.sleep(tts)

    exit(0)