#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
TashProcessGuard -- A python watchdog application designed
to automatically control the lifetime of any given process.

Copyright: 2018, Michael Flau <michael@flau.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import psutil
import subprocess
import time
import sys
import pathlib
import signal
from os import listdir
from os.path import isfile, join
from os import remove
from os import _exit
from os import kill
from screeninfo import get_monitors
from math import floor

"""
Simple signal handler
"""
class KillingMeSoftly(object):
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_graceful)
        signal.signal(signal.SIGTERM, self.exit_graceful)
        signal.signal(signal.SIGABRT, self.exit_graceful)
        # signal.signal(signal.SIGKILL, self.exit_graceful)

    def exit_graceful(self, signum, frame):
        self.kill_now = True

"""
Implements convenience logic to administrate process instances
which are supposed to run continously without interruption.
"""
class TashProcess(object):
    """
    """
    def __init__(self, procName, cmd, pidFilePath, pos=(100,40,0,0)):
        self.m_process          = None
        self.m_returnCode       = None
        self.m_processName      = procName
        self.m_pid              = 0
        self.m_cmdString        = cmd
        self.m_pidFilePath      = pidFilePath
        self.m_pidFileName      = ""
        self.m_terminalPosition = pos
    """
    Start the child process inhabited
    """
    def start(self):
        # Generate terminal position string
        posstr = "{}x{}+{}+{}".format(self.m_terminalPosition[0],
                                      self.m_terminalPosition[1],
                                      self.m_terminalPosition[2],
                                      self.m_terminalPosition[3])
        # Generate start command
        startp = ["/usr/bin/xterm",
                  "-fa", "Monospace",
                  "-fs","8",
                  "-geometry",posstr,
                  "-rv",
                  # "-hold",
                  "-T", self.m_processName.split('/')[-1],
                  "-e"] + self.m_cmdString

        self.m_process = subprocess.Popen(startp)
        self.m_pid = self.m_process.pid
        self.m_pidFileName = writePIDFile(self.m_pidFilePath, self.m_pid, self.m_cmdString)
    """
    Stop the child process inhabited
    """
    def stop(self, delete = False):
        assert self.m_process != None

        notkilled = True
        killcnt = 0
        while notkilled:
            try:
                print("Killing {}...".format(self.m_processName))
                self.m_process.wait(1)
            except subprocess.TimeoutExpired:
                print("Timeout expired on gently killing {}, retry...!".format(self.m_processName))
                notkilled = True
                killcnt += 1
                if killcnt >= 3:
                    print("{} failed attempts to shutdown {}, forcing termination now!".format(killcnt, self.m_processName))
                    notkilled = False
                    self.m_process.kill()
            else:
                print("Successfully killed {}".format(self.m_processName))
                notkilled = False

        self.m_returnCode = self.m_process.poll()
        print("{} ({}) ended with rc {}".format(self.m_processName, self.m_pid, self.m_returnCode))
        self.reset(delete=delete)

    """
    Getter property to check if the inhabited child process is still running
    """
    @property
    def running(self):
        self.m_process.poll()
        self.m_returnCode = self.m_process.returncode

        ret = False

        if self.m_returnCode == None:
            ret = True

        # print("Process return code is {}".format(self.m_returnCode))
        return ret

    """
    Reset method to set a process instance back to creation state
    """
    def reset(self, delete = False):
        if delete:
            deletePIDFile(self.m_pidFilePath, self.m_pidFileName)
        self.__init__(self.m_processName, self.m_cmdString, self.m_pidFilePath, self.m_terminalPosition)

    """
    Getter property to return the name of the inhabited process
    """
    @property
    def pname(self):
        return self.m_processName.split('/')[-1]
    """
    Getter property to return the process ID of the inhabited process
    """
    @property
    def pid(self):
        return self.m_pid

"""
Checker function to determine if a certain pid file
of a given process exists
"""
def isPIDFile(path):
    with pathlib.Path(path) as p:
        assert p.exists()
        return p.is_file()
"""
Write a PID file to stated path
"""
def writePIDFile(path, pid, processcmd):

    if not pathlib.Path(path).exists():
        pathlib.Path(path).mkdir(mode=0o777,parents=True)

    if path[-1] != '/':
        path += '/'

    filename = "{}.pid".format(pid)

    c = ""

    for part in processcmd:
        c += part+'#'

    with open("{}{}".format(path, filename),'wt') as fid:
        fid.write("{}".format(c))

    return filename

"""
Delete a PID file from a stated path
"""
def deletePIDFile(path, filename):
    if path[-1] is not '/':
        path += '/'

    # print('Checking if pid file {} exists...'.format(path+filename))

    assert isPIDFile(path+filename)
    remove(path+filename)

"""
Get a list of all pid file present in given directory
"""
def readPIDFiles(path):
    # TODO: Add check if given path is a directory
    #Only read pid files
	files = [f for f in listdir(path) if isfile(join(path, f)) and f[-3:] == "pid"]
	return files

"""
Based on exisiting PID files in given directory,
try to terminate running processes. This is the only way to
get control over started process from a new application lifecycle.
"""
def killOldProcessInstances(path):
    # TODO: Add check if given path is a directory
    files = readPIDFiles(path)

    for file in files:
        pids = file.split('.')[0]
        print("Checking PID {}".format(pids))

        try:
            kill(int(pids), 0)
        except OSError:
            print("PID file has no running process, ignoring...")
        else:
            print("PID file has an associated process, killing...")
            kill(int(pids),15)

"""
Parse given module file for lines which describe
a child application to start. Lines with a '#' will
be ignored as comments.
"""
def parseModuleFile(location):
# TODO: Check if given module file exists
    with open(location) as f:

        line = None
        cmds = []
        while line is not "":

            line = f.readline()

            if len(line) <= 0:
                continue

            if line[0] == "#":
                continue

            if line == "\n":
                continue

            cleanline = line.replace("\n","")
            cmd  = cleanline.split(" ")
            cmds.append(cmd)

        return cmds

"""
Interprets the content of a given PID file and
returns a string containing the command to start a new instance
of the very same child program.
"""
def parsePIDFiles(path):
    # TODO: Add check if given directory is valid

    #Get list of PID files
    files       = readPIDFiles(path)
    cmdstrings  = []
    commands    = []

    #Read first line of each PID file
    for file in files:
        with open(path+file,'rt') as f:
            cmdstrings.append(f.readline())

    #Filter read list for unique file names
    unique_cmdstrings = []

    for cmdstring in cmdstrings:
        skip = False

        for ucmdstring in unique_cmdstrings:
            if ucmdstring == cmdstring:
                skip = True

        if skip:
            continue

        unique_cmdstrings.append(cmdstring)

    # Split up command strings into parts, and store retrieved list
    # In a list to return to caller
    for cmdstring in unique_cmdstrings:
            commands.append(cmdstring.split('#'))

    return commands

"""
Compute terminal positions for all child processes
"""
def computeTerminalPositions(xs, ys, w, h, fs, numprocs):

    positions = []

    colidx    = 0
    rowidx    = 0
    monitor_w = 0
    monitor_h = 0

    w_actual = w * fs
    h_actual = h * fs

    monitors = get_monitors()
    num_monitors = len(monitors)

    for m in monitors:
        if m.x == 0 and m.y == 0:
            monitor_w = m.width
            monitor_h = m.height

    assert monitor_h > h_actual
    assert monitor_w > w_actual

    terms_per_col = int(floor(monitor_h / h))
    terms_per_row = int(floor(monitor_w / w))

    for termidx in range(numprocs):
        pos = (w, h, xs + colidx * w_actual, ys + (rowidx * (h_actual+100)))

        rowidx += 1

        if rowidx > terms_per_row:
            rowidx = 0
            colidx += 1

            if colidx > terms_per_col:
                colidx = 0

        positions.append(pos)

    return positions

"""
Clear screen
"""
def cls():
    print('\x1b[2J')

"""
Write a guard file to given directory
"""
def writeGuardFile(path):
    #TODO: check input path if valid
    p = path

    if p[-1] != '/':
        p += '/'

    with open(p+'guard.ts','wt') as f:
        f.write(str(int(time.time())))

"""
Check if input path is valid
"""
def fileExists(fullpath):
    with pathlib.Path(path) as p:
        return p.is_file()

"""
Remove guard file from given directory
"""
def deleteGuardFile(path):
    # TODO: check input path if valid
    p = path

    if p[-1] != '/':
        p += '/'

    f = p+'guard.ts'

    if fileExists(f):
        remove(f)

"""
Print function for printing one specific table row
"""
def printTableRow(fields):

    header_str = "|"

    for field in fields:
        header_str += '{}'.format(field[0].center(len(field[0])+field[1],' '))
        header_str += '|'

    return header_str

"""
Print function for printing the whole update tables in terminal window.
It computes a matching layout based on the longest string for each column,
applying it to all rows afterwards. This guarantees a dynamically neat table
spacing for all rows and the header.
"""
def printTable(header_fields, rows):

    #Table title
    title = 'Guarded Processes'
    #Default spacing
    def_padding = 2

    #compute padding for each field based
    #on longest string in each column

    #make sure rows and header have same amount of columns
    assert len(header_fields) == len(max(rows,key=len))

    #Initialize a vector with as many columns as header fields
    column_paddings = [None] * len(header_fields)

    #Store lenght of each header field in column padding for a start
    for hf,idx in zip(header_fields, range(len(header_fields))):
        column_paddings[idx] = int(len(hf)) + def_padding

    #Go over each row and update column paddings as greater field lengths occur
    for row in rows:
        for rf,rf_idx in zip(row, range(len(row))):
            current_padding = column_paddings[rf_idx]
            new_padding = int(len(rf))
            column_paddings[rf_idx] = new_padding if new_padding > current_padding else current_padding

    #Merge header field and respective column padding to tuple, and store in list
    next_row = []

    for hf, ht_idx in zip(header_fields, range(len(header_fields))):
        next_row.append([hf, int(column_paddings[ht_idx] - len(hf)) + def_padding])

    header = printTableRow(next_row)

    #print title
    print('-' * len(header))
    title_padding = int((len(header)-len(title))-def_padding)
    print('|{}|'.format(title.center(len(title) + title_padding,' ')))
    #print header
    print('-' * len(header))
    print(header)
    print('-' * len(header))

    #Print rows
    for row in rows:
        next_row = []
        for rf, rf_idx in zip(row, range(len(row))):
            next_row.append([rf, int(column_paddings[rf_idx] - len(rf)) + def_padding])

        print( printTableRow(next_row) )



"""
Main execution part
"""
if __name__ == "__main__":

    """
    Holds all started process objects
    """
    started = []
    """
    Directory where all pid files + guard file is stored
    """
    path    = "/tmp/tashtego_pids/"
    """
    Idle time between 2 process checks
    """
    idle    = 4
    """
    Create a Signalhandler instance, which handles incoming system interrupts
    """
    killer = KillingMeSoftly()

    try:

        """
        Get module file path from cli
        """
        if len(sys.argv) < 2:
            print('Loading state from pid files...')
            #Kill old processes first
            killOldProcessInstances(path);
            #Parse commands from pid files
            commands = parsePIDFiles(path)
        else:
            deleteGuardFile(path)
            modulepath = sys.argv[1]
            print("Reading module file from path: " + modulepath)
            commands = parseModuleFile(modulepath)

        #Get list of all existing pid files
        pidfiles = readPIDFiles(path)
        """
        Remove pid files which are obsolete
        """
        for file in pidfiles:
            deletePIDFile(path, file)

        """
        End if module file did not contain any process to be started, or if
        pid file directory didn't contain any pid files.
        """
        if len(commands) == 0:
            print("No process entries found in module file, terminating...")
            deleteGuardFile(path)
            exit(0)

        """
        Compute position for each terminal window to be displayed
        """
        positions = computeTerminalPositions(1, 0, 80, 10, 8, len(commands))

        """
        Start all processes found
        """
        for command, position in zip(commands, positions):
            print("starting {} with {}".format(command[0], command))
            proc = TashProcess(command[0], command, path, position)
            proc.start()
            started.append(proc)
            time.sleep(1)

        #Clear screen
        cls()

        """
        Enter process check loop
        1. Updates guard file
        2. Writes table header
        3. Loop over each started process and check it's state
        4. Print table row with process information 
        """
        while 1:
            #Update guard file
            writeGuardFile(path)
            rows = []

            """
            Iterate over all process instances and check if they are
            still running. If not, restart the particular instance.
            """
            for proc in started:

                tstr  = time.strftime("%Y\%m\%d %H:%M:%S", time.localtime())
                state = "None"

                """
                Do nothing if running, otherwise reset and start
                """
                if proc.running:
                    state = "running"
                else:
                    state = "restarting..."
                    proc.reset(delete=True)
                    proc.start()

                rows.append([tstr, proc.pname, str(proc.pid), state])

            #Clear terminal
            cls()
            #print table
            printTable(header_fields=['time', 'process', 'pid', 'state'],
                       rows=rows)
            # Knock off for 4 seconds
            time.sleep(idle)

            if killer.kill_now:
                print("Received interrupt signal...")
                # raise KeyboardInterrupt
                for proc in started:
                    print("Killing {} ({})...".format(proc.pname, proc.pid))
                    proc.stop()

                try:
                    sys.exit(0)
                except SystemExit:
                    _exit(0)

    except KeyboardInterrupt:
        print('Interrupt')

        deleteGuardFile(path)

        for proc in started:
            print("Killing {} ({})...".format(proc.pname, proc.pid))
            proc.stop(True)

        try:
            sys.exit(0)
        except SystemExit:
            _exit(0)