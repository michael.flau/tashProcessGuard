#!/bin/bash

# TashProcessGuard -- A python watchdog application designed
# to automatically control the lifetime of any given process.
#
# Copyright: 2018, Michael Flau <michael@flau.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo "Start Tash Save module"
#Create temporary pid file directory
mkdir -p /tmp/tashtego_pids
#change cwd
cd ~/tashtego/bin
#Omit termination button in window header
xfconf-query -c xfwm4 -p /general/button_layout -s "O|HM"
#Start the process guard daemon as systemd service
sudo systemctl start tashpgdaemon.service
#Start a process guard instance in a xterm window
xterm -rv -T tashProcessGuard -fa Monospace -fs 8 -geometry 80x10+200+0 -e ./tashProcessGuard.py ../cfg/tashtego_ps114_dryrun.module
echo "Done"